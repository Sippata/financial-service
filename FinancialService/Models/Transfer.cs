using System;

namespace FinancialService.Models
{
    public class Transfer
    {
        public long TransferToAccountNumber { get; set; }
        public long TransferFromAccountNumber { get; set; }
        public decimal Amount { get; set; }
        public DateTime ExecutionTime { get; set; }
    }
}
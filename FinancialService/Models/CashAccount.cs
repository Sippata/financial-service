using System;

namespace FinancialService.Models
{
    public class CashAccount
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public Guid UserId { get; set; }
        public long Number { get; set; }
        public decimal Balance { get; set; }
        public string State { get; set; } = "opened";
    }
}
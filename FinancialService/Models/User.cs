using System;

namespace FinancialService.Models
{
    public class User
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string State { get; set; } = "underacted";
    }
}
using System;
using System.Linq;
using FinancialService.BusinessLogic;
using FinancialService.Models;
using FinancialService.DTO.CashAccount;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FinancialService.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/accounts")]
    public class CashAccountController: ControllerBase
    {
        private readonly AccountRequestHandler _accountRequestHandler;
        private const string IdentityType = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier";

        private Guid UserId 
        {
            get
            {
                var identityClime = User.Claims.FirstOrDefault(
                    c => c.Type == IdentityType)?.Value;
                return Guid.Parse(identityClime);
            }
        }
        
        public CashAccountController(AccountRequestHandler accountRequestHandler)
        {
            _accountRequestHandler = accountRequestHandler;
        }

        [HttpGet]
        public IActionResult GetAccountList()
        {
            var accounts = _accountRequestHandler.GetAccountList(UserId);
            if (accounts == null)
            {
                return BadRequest();
            }
            
            return Ok(accounts);
        }

        [HttpGet("{number}")]
        public IActionResult GetAccount(long number)
        {
            CashAccount account = _accountRequestHandler.GetAccount(number, UserId);
            if (account == null)
            {
                return NotFound();
            }

            return Ok(account);
        }
        
        [HttpPost]
        public IActionResult OpenAccount()
        {
            CashAccount account = _accountRequestHandler.CreateAccount(UserId);
            return Created($"api/accounts/{account.Id}", account);
        }

        [HttpDelete("{number}")]
        public IActionResult CloseAccount(long number)
        {
            var isSuccess = _accountRequestHandler.CloseAccount(number, UserId);
            switch (isSuccess)
            {
                case AccountRequestHandler.OperationStatus.Success:
                    return Ok();
                case AccountRequestHandler.OperationStatus.NotEnoughMoney:
                    return BadRequest(
                        new
                        {
                            code = AccountRequestHandler.OperationStatus.NotEnoughMoney,
                            error = "Not enough money."
                        });
                case AccountRequestHandler.OperationStatus.AccountClosed:
                    return BadRequest(
                        new
                        {
                            code = AccountRequestHandler.OperationStatus.AccountClosed,
                            error = "Account is unavailable."
                        });
                case AccountRequestHandler.OperationStatus.UserNotOwnThisAccount:
                    return BadRequest(
                        new
                        {
                            code = AccountRequestHandler.OperationStatus.UserNotOwnThisAccount,
                            error = "The user is not the owner of this account."
                        });
                case AccountRequestHandler.OperationStatus.OperationFailed:
                    return BadRequest(
                        new
                        {
                            code = AccountRequestHandler.OperationStatus.OperationFailed,
                            error = "Something wrong. Try again."
                        });
                default:
                    throw new Exception("Unknown operation status");
            }
        }

        [HttpPatch("transfer")]
        public IActionResult TransferMoney([FromBody] TransferInfo transferInfo)
        {
            var (status, account) = _accountRequestHandler.Transfer(UserId, transferInfo);
            switch (status)
            {
                case AccountRequestHandler.OperationStatus.Success:
                    return Ok(account);
                case AccountRequestHandler.OperationStatus.NotEnoughMoney:
                    return BadRequest(
                        new
                        {
                            code = AccountRequestHandler.OperationStatus.NotEnoughMoney,
                            error = "Not enough money."
                        });
                case AccountRequestHandler.OperationStatus.AccountClosed:
                    return BadRequest(
                        new
                        {
                            code = AccountRequestHandler.OperationStatus.AccountClosed,
                            error = "Account is unavailable."
                        });
                case AccountRequestHandler.OperationStatus.UserNotOwnThisAccount:
                    return BadRequest(
                        new
                        {
                            code = AccountRequestHandler.OperationStatus.UserNotOwnThisAccount,
                            error = "The user is not the owner of this account."
                        });
                case AccountRequestHandler.OperationStatus.OperationFailed:
                    return BadRequest(
                        new
                        {
                            code = AccountRequestHandler.OperationStatus.OperationFailed,
                            error = "Something wrong. Try again."
                        });
                default:
                    throw new Exception("Unknown operation status");
            }
        }

        [HttpPatch("top_up")]
        public IActionResult TopUp([FromBody] TopUpInfo topUpInfo)
        {
            var (status, account) = _accountRequestHandler.TopUpAccount(topUpInfo, UserId);
            switch (status)
            {
                case AccountRequestHandler.OperationStatus.Success:
                    return Ok(account);
                case AccountRequestHandler.OperationStatus.AccountClosed:
                    return BadRequest(
                        new
                        {
                            code = AccountRequestHandler.OperationStatus.AccountClosed,
                            error = "Account is unavailable."
                        });
                case AccountRequestHandler.OperationStatus.UserNotOwnThisAccount:
                    return BadRequest(
                        new
                        {
                            code = AccountRequestHandler.OperationStatus.UserNotOwnThisAccount,
                            error = "The user is not the owner of this account."
                        });
                case AccountRequestHandler.OperationStatus.OperationFailed:
                    return BadRequest(
                        new
                        {
                            code = AccountRequestHandler.OperationStatus.OperationFailed,
                            error = "Something wrong. Try again."
                        });
                default:
                    throw new Exception("Unknown operation status");
            }
        }
    }
}
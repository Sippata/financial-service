using System.IdentityModel.Tokens.Jwt;
using FinancialService.BusinessLogic;
using FinancialService.DTO.Profile;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace FinancialService.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProfileController: ControllerBase
    {
        private readonly ProfileRequestHandler _profileRequestHandler;
        
        public ProfileController(ProfileRequestHandler profileRequestHandler)
        {
            _profileRequestHandler = profileRequestHandler;
        }
        
        [HttpPost("registration")]
        public IActionResult Register(
            [FromBody] Registration registration, 
            [FromServices] IOptions<ApiBehaviorOptions> apiBehaviorOptions)
        { 
            
            if (!_profileRequestHandler.IsEmailUniq(registration.Email))
            {
                ModelState.AddModelError("Email", "This email is already taken.");
            }
            if(!ModelState.IsValid)
            {
                return apiBehaviorOptions.Value.InvalidModelStateResponseFactory(ControllerContext);
            }
            
            _profileRequestHandler.CreateUser(ref registration);
            return Ok();
        }
        
        [HttpPost("token")]
        public IActionResult GetToken([FromBody] Authentication authModel)
        {
            var token = _profileRequestHandler.CreateToken(authModel);       
            if (token != null) {
                return Ok(new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token),
                    expiration = token.ValidTo
                });
            }

            return Unauthorized();
        }
    }
}
using System.ComponentModel.DataAnnotations;

namespace FinancialService.DTO.CashAccount
{
    public class TransferInfo
    {
        [RegularExpression(@"^4\d{9}$")]
        public long TransferToNumber { get; set; }
        [RegularExpression(@"^4\d{9}$")]
        public long TransferFromNumber { get; set; }
        public decimal Amount { get; set; }
    }
}
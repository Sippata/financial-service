using System.ComponentModel.DataAnnotations;

namespace FinancialService.DTO.CashAccount
{
    public class TopUpInfo
    {
        [RegularExpression(@"4\d{9}")]
        public long AccountNumber { get; set; }
        public decimal Amount { get; set; }
    }
}
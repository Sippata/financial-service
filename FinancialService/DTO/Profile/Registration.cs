using System.ComponentModel.DataAnnotations;

namespace FinancialService.DTO.Profile
{
    public class Registration
    {
        [Required]
        [MaxLength(30)]
        [EmailAddress]
        public string Email { get; set; }
        
        [MinLength(3)]
        [MaxLength(30)]
        public string Username { get; set; }
        
        [Required]
        [MinLength(6)]
        public string Password { get; set; }
        
        [Required]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
    }
}
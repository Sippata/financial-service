using System.ComponentModel.DataAnnotations;

namespace FinancialService.DTO.Profile
{
    public class Authentication
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using FinancialService.DTO.CashAccount;
using FinancialService.Models;
using FinancialService.Services.Interfaces;

namespace FinancialService.BusinessLogic
{
    public class AccountRequestHandler
    {
        private readonly IAccountService _accountService;

        public AccountRequestHandler(IAccountService accountService)
        {
            _accountService = accountService;
        }

        public (OperationStatus, CashAccount) Transfer(Guid userId, in TransferInfo info)
        {
            var transferFromAccount = _accountService.GetAccount(userId, info.TransferFromNumber);
            if (transferFromAccount == null)
            {
                return (OperationStatus.UserNotOwnThisAccount, null);
            }
            if (transferFromAccount.State == "closed")
            {
                return (OperationStatus.AccountClosed, null);
            }
            if (transferFromAccount.Balance < info.Amount)
            {
                return (OperationStatus.NotEnoughMoney, null);
            }

            try
            {
                _accountService.Transfer(
                    info.TransferFromNumber,
                    info.TransferToNumber, info.Amount
                );
                return (
                    OperationStatus.Success,
                    _accountService.GetAccount(userId, info.TransferFromNumber)
                    );
            }
            catch (Exception)
            {
                return (OperationStatus.OperationFailed, null);
            }
        }

        public OperationStatus CloseAccount(long number, in Guid userId)
        {
            var account = _accountService.GetAccount(userId, number);
            if (account == null)
            {
                return OperationStatus.UserNotOwnThisAccount;
            }
            if (account.State == "closed")
            {
                return OperationStatus.AccountClosed;
            }
            try
            {
                _accountService.CloseAccount(number);
                return OperationStatus.Success;
            }
            catch (Exception)
            {
                return OperationStatus.OperationFailed;
            }
        }

        public CashAccount CreateAccount(Guid userId)
        {
            CashAccount account = new CashAccount
            {
                Balance = decimal.Zero,
                Number = GetRandomAccountNumber(),
                State = "open",
                UserId = userId,
            };
            _accountService.OpenAccount(account);
            return account;
        }

        private long GetRandomAccountNumber()
        {
            var accountNumbers = _accountService.GetAllAccountNumbers()
                .ToList();
            var randomGenerator = new Random();
            long newNumber;

            while (true)
            {
                newNumber = 0;
                for (int i = 0; i < 9; i++)
                {
                    newNumber += (long) Math.Pow(10, i) * randomGenerator.Next(10);
                }

                newNumber += 4_000_000_000;
                if (accountNumbers.All(number => number != newNumber))
                {
                    break;
                }
            }

            return newNumber;
        }

        public (OperationStatus, CashAccount) TopUpAccount(TopUpInfo topUpInfo, Guid userId)
        {
            var account = _accountService.GetAccount(userId, topUpInfo.AccountNumber);
            if (account == null)
            {
                return (OperationStatus.UserNotOwnThisAccount, null);
            }

            if (account.State == "closed")
            {
                return (OperationStatus.AccountClosed, null);
            }
            account.Balance += topUpInfo.Amount;
            try
            {
                _accountService.UpdateAccount(account);
                return (
                    OperationStatus.Success, 
                    _accountService.GetAccount(userId, topUpInfo.AccountNumber)
                    );
            }
            catch (Exception)
            {
                return (OperationStatus.OperationFailed, null);
            }
        }

        public IEnumerable<CashAccount> GetAccountList(Guid userId)
        {
            return _accountService.GetAccounts(userId)
                .Where(account => account.State == "open");
        }

        public CashAccount GetAccount(long accountNumber, Guid userId)
        {
            return _accountService.GetAccount(userId, accountNumber);
        }
        
        public enum OperationStatus
        {
            Success,
            UserNotOwnThisAccount,
            NotEnoughMoney,
            AccountClosed,
            OperationFailed
        }
    }
}
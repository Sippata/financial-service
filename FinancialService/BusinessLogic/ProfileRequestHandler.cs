using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using FinancialService.DTO.Profile;
using FinancialService.Models;
using FinancialService.Services.Interfaces;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace FinancialService.BusinessLogic
{
    public class ProfileRequestHandler
    {
        private readonly IUserService _userService;
        private readonly AuthOptions _authOptions;

        public ProfileRequestHandler(IUserService userService,  IOptions<AuthOptions> authOptionsAccessor)
        {
            _userService = userService;
            _authOptions = authOptionsAccessor.Value;
        }
        
        private bool IsValidUser(User user, string password)
        {
            if (user == null || user.State == "removed")
            {
                return false;
            }

            return PasswordHash.CheckPassword(password, user.Password);
        }

        public JwtSecurityToken CreateToken(Authentication authModel)
        {
            var user = _userService.GetByEmail(authModel.Email);

            if (!IsValidUser(user, authModel.Password))
            {
                return null;
            }
            
            var authClaims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, user.State),
            };
                
            var token = new JwtSecurityToken(
                issuer: _authOptions.Issuer,
                audience: _authOptions.Audience,
                expires: DateTime.Now.AddMinutes(_authOptions.ExpiresInMinutes),
                claims: authClaims,
                signingCredentials: new SigningCredentials(
                    new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_authOptions.SecureKey)), 
                    SecurityAlgorithms.HmacSha256Signature)
            );
            return token;
        }
        
        public bool IsEmailUniq(string email)
        {
            var user = _userService.GetByEmail(email);
            return user == null;
        }
        
        public void CreateUser(ref Registration user)
        {
            _userService.AddUser(new User
            {
                Email = user.Email,
                Username = user.Username,
                Password = PasswordHash.Generate(user.Password)
            });
        }
    }
}
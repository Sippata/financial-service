CREATE TABLE users (
    id          uuid        NOT NULL,
    email       varchar(30) NOT NULL,
    username    varchar(30) NOT NULL,
    password    varchar(48),
    state       varchar(30) NOT NULL,
    
    CONSTRAINT userPk PRIMARY KEY (id),
    CONSTRAINT userAk UNIQUE (email)
);

CREATE TABLE account (
    id          uuid        NOT NULL,
    userId      uuid        NOT NULL,
    number      bigint      NOT NULL,
    balance     numeric     NOT NULL,
    state       varchar(30) NOT NULL,
    
    CONSTRAINT accountPk PRIMARY KEY (id),
    CONSTRAINT accountAk UNIQUE (number),
    CONSTRAINT accountToUserFk FOREIGN KEY (userId) REFERENCES users (id)
);


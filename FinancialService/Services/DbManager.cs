using System;
using System.Transactions;
using Microsoft.Extensions.Options;
using Npgsql;

namespace FinancialService.Services
{
    public class DbManager
    {
        private readonly string _connStr;

        public DbManager(IOptions<ConnectionConfig> connectionConfig)
        {
            _connStr = connectionConfig.Value.ConnectionString;
            Dapper.DefaultTypeMap.MatchNamesWithUnderscores = true;
        }
        
        public void ExecuteCommand(Action<NpgsqlConnection> task)
        {
            using (var conn = new NpgsqlConnection(_connStr))
            {
                conn.Open();
                task(conn);
            }
        }
        public T ExecuteCommand<T>(Func<NpgsqlConnection, T> task)
        {
            using (var conn = new NpgsqlConnection(_connStr))
            {
                conn.Open();
                return task(conn);
            }
        }

        public void ExecuteTransaction(Action<NpgsqlConnection> task)
        {
            using (var transaction = new TransactionScope())
            {
                using (var conn = new NpgsqlConnection(_connStr))
                {
                    conn.Open();
                    task(conn);
                }
                transaction.Complete();
            }
        }
    }
}
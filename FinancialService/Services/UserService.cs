using System;
using Dapper;
using FinancialService.Models;
using FinancialService.Services.Interfaces;

namespace FinancialService.Services
{
    public class UserService: IUserService
    {
        private readonly DbManager _dbManager;

        public UserService(DbManager dbManager)
        {
            _dbManager = dbManager;
        }
        public User GetById(Guid id)
        {
            return _dbManager.ExecuteCommand(conn => conn.QuerySingleOrDefault<User>(
                "SELECT id, username, email, password, state FROM users WHERE id = @id;",
                new
                {
                    id
                })
            );
        }

        public User GetByEmail(string email)
        {
            return _dbManager.ExecuteCommand(conn => conn.QuerySingleOrDefault<User>(
                "SELECT id, email, username, password, state FROM users WHERE email = @email",
                new
                {
                    email
                })
            );
        }

        public void AddUser(User user)
        {
            _dbManager.ExecuteCommand(conn =>
                {
                    conn.Execute(
                        @"INSERT INTO users (id, email, username, password, state)
                        VALUES (@Id, @Email, @Username, @Password, @State);",
                        user);
                }
            );
        }

        public void RemoveUser(Guid id)
        {
            _dbManager.ExecuteCommand(conn =>
                {
                    conn.Execute(
                        "UPDATE users SET state = 'removed' WHERE id = @id;",
                        new
                        {
                            id
                        });
                }
            );
        }

        public void UpdateUser(User user)
        {
            _dbManager.ExecuteCommand(conn =>
            {
                conn.Execute(
                    @"UPDATE users 
                    SET email = @Email, password = @Password, username = @Username, state = @State
                    WHERE id = @Id",
                    user);
            });
        }
    }
}
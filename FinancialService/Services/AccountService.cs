using System;
using System.Collections.Generic;
using Dapper;
using FinancialService.Models;
using FinancialService.Services.Interfaces;

namespace FinancialService.Services
{
    public class AccountService: IAccountService
    {
        private readonly DbManager _dbManager;

        public AccountService(DbManager dbManager)
        {
            _dbManager = dbManager;
        }
        public CashAccount GetById(Guid id)
        {
            return _dbManager.ExecuteCommand<CashAccount>(
                conn => conn.QuerySingleOrDefault(
                    "SELECT id, userid, number, balance, state FROM account WHERE id = @id;",
                    new
                    {
                        id
                    }
            ));
        }

        public CashAccount GetByNumber(long number)
        {
            return _dbManager.ExecuteCommand<CashAccount>(
                conn => conn.QuerySingleOrDefault(
                    @"SELECT id, userid, number, balance, state FROM account 
                            WHERE number = @number",
                    new
                    {
                        number
                    })
                );
        }

        public IEnumerable<CashAccount> GetAccounts(Guid userId)
        {
            return _dbManager.ExecuteCommand(
                conn => conn.Query<CashAccount>(
                    "SELECT * FROM account WHERE userId = @userId;",
                    new
                    {
                        userId
                    })
                );
        }

        public IEnumerable<long> GetAllAccountNumbers()
        {
            return _dbManager.ExecuteCommand(
                conn => conn.Query<long>(
                    "SELECT number FROM account;")
            );
        }

        public CashAccount GetAccount(Guid userId, Guid id)
        {
            return _dbManager.ExecuteCommand(
                conn => conn.QuerySingleOrDefault<CashAccount>(
                    @"SELECT a.id, a.userid, a.number, a.balance, a.state
                            FROM account as a 
                            WHERE a.number = @id AND a.userid = @userId;",
                    new
                    {
                        id,
                        userId,
                    })
            );
        }

        public void OpenAccount(CashAccount cashAccount)
        {
            _dbManager.ExecuteCommand(conn =>
            {
                conn.Execute(
                    "INSERT INTO account VALUES (@Id, @UserId, @Number, @Balance, @State)",
                    cashAccount);
            });
        }

        public void CloseAccount(long number)
        {
            _dbManager.ExecuteCommand(conn =>
            {
                conn.Execute("UPDATE account SET state = @state WHERE number = @number",
                    new
                    {
                        state = "closed",
                        number,
                    });
            });
        }

        public void UpdateAccount(CashAccount updatedAccount)
        {
            _dbManager.ExecuteCommand(conn =>
            {
                conn.Execute(
                    "UPDATE account SET balance = @Balance, state = @State WHERE id = @Id",
                    updatedAccount);
            });
        }

        public void Transfer(long transferFromNumber, long transferToNumber, decimal amount)
        {
            _dbManager.ExecuteTransaction(conn =>
            {
                conn.Execute(
                    "UPDATE account SET balance = balance - @amount WHERE number = @number;",
                    new
                    {
                        number = transferFromNumber,
                        amount,
                    });
                conn.Execute(
                    "UPDATE account SET balance = balance + @amount WHERE number = @number;",
                    new
                    {
                        number = transferToNumber,
                        amount
                    });
            });
        }

        public CashAccount GetAccount(Guid userId, long accountNumber)
        {
            return _dbManager.ExecuteCommand(
                conn => conn.QuerySingleOrDefault<CashAccount>(
                    @"SELECT a.id, a.userid, a.number, a.balance, a.state
                            FROM account as a 
                            WHERE a.number = @number AND a.userid = @userId;",
                    new
                    {
                        number = accountNumber,
                        userId,
                    })
            );
        }
    }
}
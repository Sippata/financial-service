using System;
using FinancialService.Models;

namespace FinancialService.Services.Interfaces
{
    public interface IUserService
    { 
        User GetById(Guid id);
        User GetByEmail(string email);
        void AddUser(User user);
        void RemoveUser(Guid id);
        void UpdateUser(User user);
    }
}
using System;
using System.Collections.Generic;
using FinancialService.Models;

namespace FinancialService.Services.Interfaces
{
    public interface IAccountService
    {
        CashAccount GetById(Guid id);
        CashAccount GetByNumber(long number);
        IEnumerable<CashAccount> GetAccounts(Guid userId);
        IEnumerable<long> GetAllAccountNumbers();
        CashAccount GetAccount(Guid userId, Guid accountId);
        CashAccount GetAccount(Guid userId, long accountNumber);
        void OpenAccount(CashAccount cashAccount);
        void CloseAccount(long number);
        void UpdateAccount(CashAccount updatedAccount);
        void Transfer(long transferFromNumber, long transferToNumber,  decimal amount);
    }
}